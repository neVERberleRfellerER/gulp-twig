var Twig = require('twig');
var map = require('map-stream');
var gutil = require('gulp-util');
var path = require('path');

module.exports = function (data) {
    "use strict";

    Twig.cache(false);

    return map(function compileTwig(file, callback) {
        if (file.isStream()) {
            this.emit('error',
                      new gutil.PluginError('gulp-twig',
                                            'Streaming not supported'));
            callback();
        }

        var fileData = data || {};

        if (typeof data === 'function') {
            fileData = fileData(file);
        }

        Twig.twig({
            path: file.path,
            base: file.base,
            load: function (template) {
                file.contents = new Buffer(template.render(fileData), 'utf-8');
                file.path = gutil.replaceExtension(file.path, '');
                callback(null, file);
            }
        });
    });
};
